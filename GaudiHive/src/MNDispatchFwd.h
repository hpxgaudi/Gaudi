/***********************************************************************************\
* (c) Copyright 2023-2023 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/
#ifndef MNDISPATCHFWD_H_
#define MNDISPATCHFWD_H_

// Gaudi
#include "GaudiKernel/StatusCode.h"

// Standard library
#include <tuple>

namespace MNDispatch {
  // Forward declarations
  std::tuple<StatusCode::ErrorCode, int, bool, bool> dispatchEvent_wk( int eventIdx ); // StatusCode, number of finished
                                                                                 // events, whether the event was scheduled,
										 // and whether the loop is done
  std::tuple<StatusCode::ErrorCode, int> drainScheduler_wk(); // StatusCode, number of finished events
  void                                   stopWorker_wk();
} // namespace MNDispatch

#endif // MNDISPATCHFWD_H_
