/***********************************************************************************\
* (c) Copyright 2023-2023 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/
#ifndef GAUDIHIVE_MNDISPATCH_H_
#define GAUDIHIVE_MNDISPATCH_H_
/// Deals with HPX machinery to dispatch to other nodes

// Gaudi includes
#include "GaudiKernel/StatusCode.h"

// HPX includes
#include <hpx/hpx.hpp>

// Standard library
#include <tuple>

// Forward declare event loop manager class
class HiveSlimMNEventLoopMgr;

namespace MNDispatch {
  void registerEventLoopMgr( HiveSlimMNEventLoopMgr* evtLoopMgr );

  // Functions run on master locality to wrap HPX functionality
  StatusCode dispatchEvent( int eventIdx, int& finished, bool& done );
  StatusCode drainSchedulers( int& finished );
  void       stopWorkers();

  // Functions to define HPX actions on this locality
  std::tuple<StatusCode::ErrorCode, int, bool, bool> dispatchEvent_wk( int eventIdx ); // StatusCode, number of finished
                                                                                 // events, whether the event was scheduled, 
										 // and whether the loop is done
  std::tuple<StatusCode::ErrorCode, int> drainScheduler_wk(); // StatusCode, number of finished events
  void                                   stopWorker_wk();
} // namespace MNDispatch

// Declare HPX actions
HPX_DECLARE_PLAIN_ACTION( MNDispatch::dispatchEvent_wk, MND_dispatchEvent_action );
HPX_DECLARE_PLAIN_ACTION( MNDispatch::drainScheduler_wk, MND_drainScheduler_action );
HPX_DECLARE_PLAIN_ACTION( MNDispatch::stopWorker_wk, MND_stopWorker_action );

#endif // GAUDIHIVE_MNDISPATCH_H_
