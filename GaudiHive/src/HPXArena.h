#ifndef HPXARENA_H_
#define HPXARENA_H_
#include <hpx/async_local/apply.hpp>

class HPXArena {
public:
  template <typename F>
  void enqueue( F&& f ) {
    hpx::apply( f );
  }
};

#endif // HPXARENA_H_
