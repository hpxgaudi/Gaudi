#ifndef PROGRAM_INVOCATION_NAME_H_
#define PROGRAM_INVOCATION_NAME_H_
#define _GNU_SOURCE /* See feature_test_macros(7) */
#include <errno.h>

extern char* program_invocation_name;
extern char* program_invocation_short_name;

#endif // PROGRAM_INVOCATION_NAME_H_
