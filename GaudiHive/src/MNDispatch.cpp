/***********************************************************************************\
* (c) Copyright 2023-2023 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/
#include "MNDispatch.h"

// Gaudi includes
#include "GaudiKernel/IEvtSelector.h"
#include "GaudiKernel/IScheduler.h"
#include "GaudiKernel/MsgStream.h"
#include "HiveSlimMNEventLoopMgr.h"
#include <hpx/hpx_finalize.hpp>

// TBB includes (HPX doesn't have certain utilities)
// #include <tbb/queuing_mutex.>

// Standard includes
#include <memory>
#include <chrono>

using namespace std::literals::chrono_literals;

namespace MNDispatch {
  // On each worker, keep track of a pointer to the EventLoopManager
  HiveSlimMNEventLoopMgr* eventLoopMgr_wk = nullptr;
  IScheduler*             schedulerSvc_wk = nullptr;
  int                     numSlots        = 0;

  namespace {
    // unnamed namespace for utilities
    MsgStream& debug() { return eventLoopMgr_wk->debug(); }
    MsgStream& info() { return eventLoopMgr_wk->info(); }
    MsgStream& error() { return eventLoopMgr_wk->error(); }
  } // namespace
  // Prevent multiple actions running at once on a worker
  hpx::mutex actionMtx_wk{};
  // Keep track of whether this worker is initialized (first event is complete)
  // 0 -> no event yet scheduled, 1 -> first event scheduled but not drained, 2 -> fully initialized
  int initialized_wk = 0;

  void registerEventLoopMgr( HiveSlimMNEventLoopMgr* evtLoopMgrParam ) {
    schedulerSvc_wk = evtLoopMgrParam->m_schedulerSvc;
    numSlots        = evtLoopMgrParam->m_whiteboard->getNumberOfStores();
    eventLoopMgr_wk = evtLoopMgrParam;
  }

  // HPX action definitions

  void localWaitForTasks() {
    auto& tm = hpx::threads::get_thread_manager();
    // we need to wait until all HPX threads (except the current one plus all
    // background threads) have exited
    auto num_threads = std::int64_t( tm.get_background_thread_count() + 2 );
    while ( tm.get_thread_count() > num_threads ) {}
  }

  using asyncDrainScheduler_t = std::unique_ptr<hpx::future<std::tuple<StatusCode::ErrorCode, int>>>;
  asyncDrainScheduler_t asyncDrainScheduler() {
    asyncDrainScheduler_t::element_type fut = hpx::async([](){
      int finished = 0;
      StatusCode sc = eventLoopMgr_wk->drainScheduler(finished);
      return std::tuple{StatusCode::ErrorCode(sc.getCode()), finished};
    });
    return std::make_unique<decltype(fut)>(std::move(fut));
  }

  std::tuple<StatusCode::ErrorCode, int, bool, bool> dispatchEvent_wk( int eventIdx ) {
	    static const int node = hpx::get_locality_id();
    static asyncDrainScheduler_t drain = nullptr;
    int finished = 0;
    if (drain != nullptr) {
       if (drain->wait_for(10ms) == hpx::future_status::timeout) {
	 // Draining scheduled but not yet complete. Skip this locality.
	 return {StatusCode::SUCCESS, 0, false, false};
       }
       else {
	 // Draining scheduled and complete. Retrieve return values.
	 StatusCode::ErrorCode ec;
	 std::tie(ec, finished) = drain->get();
	 drain.reset();
	 if (ec == StatusCode::FAILURE) {
            error() << "A StatusCode::FAILURE has been produced on worker " << hpx::get_locality_id()
                    << " while draining the scheduler" << endmsg;
	 }
       }
    }
    // info() << "dispatchEvent_wk(" << node << "): Taking lock" << endmsg;
    std::lock_guard lock{ actionMtx_wk };

    // Drain scheduler if:
    // there are no free slots OR
    // the first event has been scheduled but the scheduler has not yet been drained
    int occupiedSlots = numSlots - schedulerSvc_wk->freeSlots();
    // info() << "dispatchEvent_wk(" << node << "): Checking for free slots. " << occupiedSlots << " are occupied." << endmsg;
    if ( eventLoopMgr_wk->m_schedulerSvc->freeSlots() <= 0 || eventLoopMgr_wk->m_whiteboard->freeSlots() <= 0 ||
         initialized_wk == 1 ) {
      drain = asyncDrainScheduler();
      initialized_wk = 2; // Record that this worker is now initialized
      // Skip this locality
      return {StatusCode::SUCCESS, 0, false, false};
    }

    if ( eventLoopMgr_wk->m_evtSelector ) {
      // Don't "jump" if we don't have an event selector
      // Move to eventIdx by rewinding (to just before the first event) then jumping eventIdx events
      StatusCode sc = eventLoopMgr_wk->m_evtSelector->rewind( *( eventLoopMgr_wk->m_evtContext ) );
      if ( sc.isFailure() ) {
        error() << "A StatusCode::FAILURE has been produced on worker " << hpx::get_locality_id()
                << " while rewinding the event selector" << endmsg;
        return { StatusCode::FAILURE, finished, true, false }; // Catastrophic failure, might as well pretend event has been scheduled
      }
      if ( eventIdx > 0 ) {
        for ( int i = 0; i < eventIdx; ++i ) {
          sc = eventLoopMgr_wk->m_evtSelector->next( *( eventLoopMgr_wk->m_evtContext ) );
          if ( sc.isFailure() ) {
            // failure in IEvtSelector::next actually means we've run out of events
            return { StatusCode::SUCCESS, finished, true, true }; // Loop done. That means the "event" was also scheduled.
          }
        }
      }
    }

    // Create the event context and schedule the event
    debug() << "dispatchEvent_wk(" << node << "): Creating event" << endmsg;
    auto ctx = eventLoopMgr_wk->createEventContext();
    if ( !ctx.valid() ) {
      return { StatusCode::SUCCESS, finished, true, true }; // Loop done
    }
    debug() << "dispatchEvent_wk(" << node << "): Running event" << endmsg;
    StatusCode sc = eventLoopMgr_wk->executeEvent( std::move( ctx ) );
    if ( sc.isRecoverable() ) { return { StatusCode::RECOVERABLE, finished, true, false }; }
    if ( sc.isFailure() ) {
      error() << "A StatusCode::FAILURE has been produced on worker " << hpx::get_locality_id()
              << " while running executeEvent" << endmsg;
      return { StatusCode::FAILURE, finished, true, false };
    }
    // If this was the first event on this worker, set initialized_wk to 1
    // so scheduler gets drained before the next event is scheduled
    if ( initialized_wk == 0 ) { initialized_wk = 1; }
    debug() << "dispatchEvent_wk(" << node << "): Done" << endmsg;
    return { StatusCode::SUCCESS, finished, true, false };
 }

  std::tuple<StatusCode::ErrorCode, int> drainScheduler_wk() {
    std::lock_guard lock{ actionMtx_wk };
    int             finished     = 0;
    int             finished_tmp = 0;
    info() << "drainScheduler_wk: Waiting until scheduler is completely free" << endmsg;
    schedulerSvc_wk->waitForEvents();
    info() << "drainScheduler_wk: Draining" << endmsg;
    do {
      finished_tmp = 0;
      if ( eventLoopMgr_wk->drainScheduler( finished_tmp ).isFailure() ) {
        error() << "A StatusCode::FAILURE has been produced on worker " << hpx::get_locality_id()
                << " while draining the scheduler" << endmsg;
        return { StatusCode::FAILURE, finished };
      }
      finished += finished_tmp;
      int occupiedSlots = numSlots - schedulerSvc_wk->freeSlots();
      info() << "drainScheduler_wk: " << finished_tmp << " more events finished, " << occupiedSlots << " / " << numSlots << " slots occupied" << endmsg;
    } while ( schedulerSvc_wk->freeSlots() < numSlots - 1 );
    localWaitForTasks();
    info() << "drainScheduler_wk: " << finished << " finished" << endmsg;
    return { StatusCode::SUCCESS, finished };
  }

  void stopWorker_wk() {
    std::lock_guard lock{ actionMtx_wk };
    info() << "Stopping worker " << hpx::get_locality_id() << endmsg;
    eventLoopMgr_wk->m_hpxStopLoop = true;
    localWaitForTasks();
  }

} // namespace MNDispatch

HPX_DEFINE_PLAIN_ACTION( MNDispatch::dispatchEvent_wk, MND_dispatchEvent_action );
HPX_DEFINE_PLAIN_ACTION( MNDispatch::drainScheduler_wk, MND_drainScheduler_action );
HPX_DEFINE_PLAIN_ACTION( MNDispatch::stopWorker_wk, MND_stopWorker_action );

namespace MNDispatch {
    long long total_tries = 0;
    long long total_events = 0;
    std::chrono::microseconds fail_latency{0};
    std::chrono::microseconds succ_latency{0};

  StatusCode dispatchEvent( int eventIdx, int& finished, bool& done ) {
    // reset done flag
    done = false;

    // Define instance of action
    MND_dispatchEvent_action dispatchEvent_action;

    // Determine locality to run on
    static const auto   localities       = hpx::find_all_localities();
    static const int    num_localities   = localities.size();
    static int   running_locality = 0;
    StatusCode sc;

    // debug() << "Submitting event " << eventIdx << " to locality " << loc_idx << " ("
    //       << loc << ")" << endmsg;
    ++total_events;
    bool submitted = false;
    while(!submitted) {
      ++total_tries;
      int loc_idx =  (running_locality++) % num_localities;
      hpx::id_type loc              = localities.at(loc_idx);
      auto start = std::chrono::system_clock::now();
      std::tie(sc, finished, submitted, done) = dispatchEvent_action( loc, eventIdx );
      auto latency = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start);
      if (submitted) {
	      succ_latency += latency;
      }
      else {
	      fail_latency += latency;
      }
    }
    return sc;
  }

  StatusCode drainSchedulers( int& finished ) {
    info() << "Draining schedulers" << endmsg;
    // Define instance of action
    MND_drainScheduler_action drainScheduler_action;

    // Run action on every locality
    auto                                                             localities = hpx::find_all_localities();
    std::vector<hpx::future<std::tuple<StatusCode::ErrorCode, int>>> results{};
    results.reserve( localities.size() );
    for ( auto loc : localities ) { results.push_back( hpx::async( drainScheduler_action, loc ) ); }
    hpx::wait_all( results );

    StatusCode ret_sc = StatusCode::SUCCESS;
    for ( auto&& fut : results ) {
      auto&& [status, n_finished] = fut.get();
      debug() << "Result: " << status << ", " << n_finished << endmsg;
      finished += n_finished;
      if ( status == StatusCode::FAILURE ) { ret_sc = StatusCode::FAILURE; }
    }
    return ret_sc;
  }

  void stopWorkers() {
    if ( hpx::get_locality_id() != 0 ) {
      // Not on master node, exit immediately
      return;
    }
    // Define instance of action
    MND_stopWorker_action stopWorker_action;

    // Only run on the remote localities
    auto                           localities = hpx::find_all_localities();
    std::vector<hpx::future<void>> results{};
    results.reserve( localities.size() );
    info() << "Stopping workers" << endmsg;
    for ( auto loc : localities ) { results.push_back( hpx::async( stopWorker_action, loc ) ); }
    hpx::wait_all( results );
    auto f = hpx::async( []() { hpx::finalize(); } );
    f.wait();
    info() << "Stopped workers" << endmsg;
    info() << "Total events: " << total_events << "\n"
	   << "Total dispatch tries: " << total_tries << "\n"
	   << "Average dispatch tries: " << total_tries/total_events << "\n"
	   << "Average fail latency: " << fail_latency.count() / (total_tries - total_events) << "us\n"
	   << "Average succ latency: " << succ_latency.count() / total_events << "us\n"
	   << "Average sub latency: " << (succ_latency + fail_latency).count() / total_events << "us" << endmsg;
  }

} // namespace MNDispatch
